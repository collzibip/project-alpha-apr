from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Project(models.Model):
    # This defines the fields of your model
    name = models.CharField(max_length=200)
    description = models.TextField()
    owner = models.ForeignKey(
        User, related_name="projects", on_delete=models.CASCADE, null=True
    )

    # This tells Django how to convert our model into a string
    # when we print() it, or when the admin displays it.
    def __str__(self):
        return self.name
